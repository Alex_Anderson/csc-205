FILE CONTROL BLOCK:
The ##FCB## is a 36-byte data structure utilized by CPM for the control of files. The file control block data consists of:
00h - The Drive, (1-16) (A-P)
01h - File Name, 7-bit(ASCII)
09h - File Type, 7-bit(ASCII)
0Ch - 'EX', set to zero when opening file
(0D and 0E are 'S1' and 'S2', reserved)
0Fh - 'RC' Set to zero  when opening file
10h - 'AL' Image of second half of directory enrty
20h -  'CR' Current record 
21h -   RAM number

We use this data structure by calling 5ch - cp/m 'prepares' it for us.
We also call F_OPEN.
With F_OPEN, on return, A = 'OFFh' (it is maxed out) is indicative of an error.
Ideally, if I was able to get the program to work, I would also be able to specify the kind of error.
This is because when A = 'OFFh', CP/M will return a hardware error in the registers H and B.